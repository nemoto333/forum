package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Report;
import com.example.demo.repository.ReportRepository;

@Service
public class ReportService {
	@Autowired
	ReportRepository reportRepository;

	public List<Report> findAllReport(String startDate, String endDate) {
		if (!startDate.isBlank()) {
			startDate += " 00:00:00";
		} else {
			startDate = "2022-01-01 00:00:00";
		}

		if (!endDate.isBlank()) {
			endDate += " 23:59:59";
		} else {
			endDate = getTime();
		}
		Timestamp start = Timestamp.valueOf(startDate);
		Timestamp end = Timestamp.valueOf(endDate);


		return reportRepository.findBycreatedDateOrderByupdatedDate(start, end);
	}

	//レコード追加
	public void saveReport(Report report) {
		if (report.getId() == 0) {
			report.setCreatedDate(Timestamp.valueOf(getTime()));
			report.setUpdatedDate(Timestamp.valueOf(getTime()));
			//report.setUpdatedDate(new Timestamp(System.currentTimeMillis()));
		} else {
			report.setUpdatedDate(Timestamp.valueOf(getTime()));
		}
		reportRepository.save(report);
	}

	//レコード削除
	public void deleteReport(Integer id) {
		reportRepository.deleteById(id);
	}

	//レコード1件取得
	public Report findReport(Integer id) {
		return reportRepository.findById(id).orElseThrow();
	}

	private String getTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

}
