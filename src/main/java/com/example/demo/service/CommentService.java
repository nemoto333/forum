package com.example.demo.service;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.entity.Comment;
import com.example.demo.repository.CommentRepository;

@Service
public class CommentService {
	@Autowired
	CommentRepository commentRepository;

	public List<Comment> findAllComment() {
		return commentRepository.findAllOrderByupdated_date();
	}

	public void saveComment(Comment comment) {
		if (comment.getId() == 0) {
			comment.setCreated_date(Timestamp.valueOf(getTime()));
			comment.setUpdated_date(Timestamp.valueOf(getTime()));
		} else {
			comment.setUpdated_date(Timestamp.valueOf(getTime()));
		}
		commentRepository.save(comment);
	}

	public Comment findComment(Integer id) {
		return commentRepository.findById(id).orElseThrow();
	}

	public void deleteComment(Integer id) {
		commentRepository.deleteById(id);
	}

	private String getTime() {
		return new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(new Date());
	}

}
