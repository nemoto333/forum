package com.example.demo.repository;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Report;

@Repository
public interface ReportRepository extends JpaRepository<Report, Integer> {
	//List<Report> findBycreatedDateBetween(Timestamp start, Timestamp end);
	@Query("select report from Report report where report.createdDate between :start and :end "
			+ "order by report.updatedDate desc")
	List<Report> findBycreatedDateOrderByupdatedDate(@Param("start") Timestamp start, @Param("end") Timestamp end);
}
