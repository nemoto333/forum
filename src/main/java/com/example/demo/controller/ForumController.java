package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.demo.entity.Comment;
import com.example.demo.entity.Report;
import com.example.demo.service.CommentService;
import com.example.demo.service.ReportService;

@Controller
public class ForumController {
	@Autowired
	ReportService reportService;
	@Autowired
	CommentService commentService;

	// 投稿内容表示画面
	@GetMapping
	public ModelAndView top(@ModelAttribute("start") String startDate, @ModelAttribute("end") String endDate) {
		ModelAndView mav = new ModelAndView();
		// 投稿を全件取得
		List<Report> contentData = reportService.findAllReport(startDate, endDate);
		//コメントを全件取得
		List<Comment> commentData = commentService.findAllComment();
		// 画面遷移先を指定
		mav.setViewName("/top");
		// 投稿データオブジェクトを保管
		mav.addObject("contents", contentData);
		mav.addObject("comments", commentData);
		return mav;
	}

	//新規投稿画面
	@GetMapping("/new")
	public ModelAndView newContent() {
		ModelAndView mav = new ModelAndView();
		// form用の空のentityを準備
		Report report = new Report();
		//画面遷移先を指定
		mav.setViewName("/new");
		mav.addObject("formModel", report);
		return mav;
	}

	//投稿処理
	@PostMapping("/add")
	public ModelAndView addContent(@ModelAttribute("formModel") Report report) {
		//投稿をテーブルに格納
		reportService.saveReport(report);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");
	}

	//投稿削除処理
	@DeleteMapping("/delete/{id}")
	public ModelAndView deleteContent(@PathVariable("id") Integer id) {
		//投稿をテーブルから削除
		reportService.deleteReport(id);
		//rootへリダイレクト
		return new ModelAndView("redirect:/");

	}

	@GetMapping("/edit/{id}")
	public ModelAndView editContent(@PathVariable("id") Integer id) {
		ModelAndView mav = new ModelAndView();
		Report report = reportService.findReport(id);
		mav.setViewName("/edit");
		mav.addObject("formModel", report);
		return mav;

	}

	@PutMapping("/update")
	public ModelAndView updateContent(@ModelAttribute("formModel") Report report) {
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	//新規コメント投稿画面
	@GetMapping("/newComment/{id}")
	public ModelAndView newComment(@PathVariable("id") Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = new Comment();
		comment.setReport_id(id);
		mav.setViewName("/newComment");
		mav.addObject("formModel", comment);
		return mav;
	}

	//コメント処理
	@PostMapping("/addComment")
	public ModelAndView addComment(@ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment);
		Report report = reportService.findReport(comment.getReport_id());
		reportService.saveReport(report);
		return new ModelAndView("redirect:/");
	}

	//コメント編集画面
	@GetMapping("/editComment/{id}")
	public ModelAndView editComment(@PathVariable("id") Integer id) {
		ModelAndView mav = new ModelAndView();
		Comment comment = commentService.findComment(id);
		mav.setViewName("/editComment");
		mav.addObject("formModel", comment);
		return mav;
	}

	@PutMapping("/updateComment")
	public ModelAndView updateComment(@ModelAttribute("formModel") Comment comment) {
		commentService.saveComment(comment);
		return new ModelAndView("redirect:/");
	}

	@DeleteMapping("/deleteComment/{id}")
	public ModelAndView deleteComment(@PathVariable("id") Integer id) {
		commentService.deleteComment(id);
		return new ModelAndView("redirect:/");
	}

}
